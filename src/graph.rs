// graph.rs
//
// Copyright 2020 Vas_Sferd <vassferd@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later


pub mod graphs {
    use std::fmt;

    #[derive(Clone, Default, Debug)]
    pub struct Edge {
        pub first: i32,
        pub second: i32,
        pub weight: i32,
    }

    impl Edge {
        pub fn new(first: i32, second: i32, weight: i32) -> Self {
            Edge {
                first,
                second,
                weight,
            }
        }
    }

    impl fmt::Display for Edge {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "({}, {}, {})", self.first, self.second, self.weight)
        }
    }

    #[derive(Clone, Default, Debug)]
    pub struct Graph {
        pub vertices: Vec<i32>,
        pub edges: Vec<Edge>,
    }

    impl Graph {
        // Добавление новой вершины
        pub fn add_vertices(&mut self, vertice: i32) {
            if self.vertices.contains(&vertice) {
                self.vertices.push(vertice)
            };
        }

        // Добавление нового ребра
        pub fn add_edge(&mut self, edge: Edge) {
            self.add_vertices(edge.first);
            self.add_vertices(edge.second);
            self.edges.push(edge);
        }
    }

    impl fmt::Display for Graph {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            let mut out_line = String::new();

            out_line.push_str("{\n");

            for edge in &self.edges {
                &out_line.push_str(edge.to_string().as_str());
                &out_line.push_str(",\n");
            };

            out_line.push_str("}\n");

            write!(f, "{}", out_line)
        }
    }
}
