// graph.rs
//
// Copyright 2020 Vas_Sferd <vassferd@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later


pub mod kruskal {
    use crate::graph::graphs::Graph;

    pub fn kruskal_algo(graph: &Graph) -> Graph {
        // Подготавливаем данные
        let tree = &mut Graph::default();
        let remaining_edges = &mut graph.edges.clone();
        remaining_edges.sort_by(|a, b| a.weight.cmp(&b.weight));

        while remaining_edges.len() > 0 {
            let added = remaining_edges.remove(0);            // Вынимаем грань с наименьшим весом
            remaining_edges.retain(|a| a.second != added.second);  // Удаляем ребра, вызывающие цикл
            tree.add_edge(added);                                   // Добавляем ребро
        }

        tree.clone()
    }
}