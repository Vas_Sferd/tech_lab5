// main.rs
//
// Copyright 2020 Vas_Sferd <vassferd@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later


mod graph;
mod graph_algorithm;
mod scanner;

use graph::graphs::*;
use graph_algorithm::kruskal::*;
use scanner::scanners::VarScanner;

fn main() {
    let my_graph = Graph {
        vertices: vec![1, 2, 3, 4, 5, 6],
        edges: vec![
            Edge::new(5, 2, 3),
            Edge::new(6, 2, 2),
            Edge::new(6, 3, 4),
            Edge::new(3, 4, 8),
            Edge::new(2, 4, 1),
            Edge::new(4, 1, 5),
            Edge::new(1, 2, 9),
        ]
    };

    let tree = kruskal_algo(&my_graph);

    println!("tree : {}", tree);

    let mut user_graph = Graph::default();
    let mut sc = VarScanner::new(" ".to_string());
    let n: i32 = sc.scan();

    for _ in 0 .. n {
        &user_graph.add_edge(
            Edge::new(sc.scan(), sc.scan(), sc.scan())
        );
    }

    let user_graph = kruskal_algo(&user_graph);

    println!("user_tree : {}", user_graph);
}
