// main.rs
//
// Copyright 2020 Vas_Sferd <vassferd@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later


pub mod scanners {
    use std::io::{Stdin, stdin};

    pub struct VarScanner<T: Default> {
        buffer: Vec<T>,
        delimiter_str: String,
        source: Stdin,
    }

    impl VarScanner<i32> {
        pub fn new(delimiter_str: String) -> Self {
            Self {
                buffer: Vec::new(),
                delimiter_str,
                source: stdin(),
            }
        }

        pub fn scan(&mut self) -> i32 {
            if self.buffer.len() == 0 {
                let mut string_buffer = String::new();
                self.source.read_line(&mut string_buffer).expect("Scanner::Error IO!\n");

                // Заполняем буффер
                self.buffer.append(&mut string_buffer
                    .split(&self.delimiter_str)
                    .map(|el| el.trim().parse::<i32>().unwrap_or(0))
                    .collect::<Vec<i32>>()
                );
            }

            self.buffer.remove(0)
        }
    }
}